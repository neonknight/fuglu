# VSCode Fuglu docker setup

- make sure VSCode extension "Dev Containers" (`ms-vscode-remote.remote-containers`) is installed
- in the followin examples it has been assumed the fuglu repository is checked out to the directory `/path/to/your/git/workspaces`
- from the current directory copy `.devcontainer` and `.vscode` into your fuglu repo `/path/to/your/git/workspaces/fuglu`
- edit the `fuglu` service in `/path/to/your/git/workspaces/fuglu/.devcontainer/.docker-compose.yml`
  - Setup USERNAME, UID and GID to match values of your user on the host system
  - Enable and define target for mounting ssh keys (for connecting to git repos using ssh if ssh-agent connection doesn't work)
- open fuglu folder in VScode (`cd /path/to/your/git/workspaces/fuglu && code .`)
- When being asked "Folder contains a Dev Container configuration file. Reopen folder to develop in a container (learn more)." click on "Reopen in container".
- What you get:
  - Run configurations to `run`, `lint` and `debug file`
  - Working test setup (you might have to open a file containing tests to get the testing symbol in the VSCode bar)
  - RUST as the tool for checking code and pep8 conformity
