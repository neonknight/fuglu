ARG BASEIMAGE=debian:stable-slim
FROM $BASEIMAGE as fuglu-testenv-py3

ARG USERNAME=develop
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN sed -i 's/Components: main/Components: main contrib non-free/g' /etc/apt/sources.list.d/debian.sources
RUN apt update && apt -y --no-install-recommends install python3 python3-distro \
    python3-lxml python3-bs4 python3-chardet python3-dnspython python3-geoip2 \
    python3-mysqldb python3-sqlalchemy python3-redis python3-yaml \
    python3-srs python3-spf python3-dkim python3-authres python3-nacl \
    python3-tz python3-socks python3-pyparsing python3-pil python3-zbar \
    python3-cryptography python3-pykcs11 python3-asn1crypto python3-oscrypto python3-bcrypt python3-paramiko python3-pycparser \
    python3-magic python3-py7zr python3-ssdeep python3-rarfile unrar \
    python3-aiohttp python3-elasticsearch \
    python3-mock python3-pytest netcat-openbsd python3-sphinx make \
    # development
    python3-pycodestyle python3-pyflakes pyupgrade python3-flake8-builtins python3-pep8-naming \
    && apt clean && rm -rf /var/lib/apt/lists

FROM fuglu-testenv-py3 AS fuglu-prepare-venv
RUN apt update && \
    apt -y --no-install-recommends install wget gcc libc6-dev python3-dev python3-wheel python3-setuptools python3-pip python3-venv unzip \
    && apt clean && rm -rf /var/lib/apt/lists
RUN /usr/bin/python3 -m venv --system-site-packages /fugluenv
RUN /fugluenv/bin/pip3 install dmarc endesive \
    # development
    flake8-annotations flake8-bugbear flake8-commas flake8-simplify flake8-async
RUN /fugluenv/bin/pip3 install https://github.com/crustymonkey/python-libmilter/archive/python3.zip
RUN wget https://gitlab.com/fumail/domainmagic/-/archive/master/domainmagic-master.zip && \
    unzip -e domainmagic-master.zip && cd domainmagic-master/ && /fugluenv/bin/python3 -m pip install . --force && \
    cd - && rm -rf domainmagic-master domainmagic-master.zip

# copy venv
FROM fuglu-testenv-py3 as fuglu-py3-source
COPY --from=fuglu-prepare-venv /fugluenv /fugluenv

# Create the user
RUN groupadd -f --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    #
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME \
    && apt install -y git

# Set the default user
USER $USERNAME
