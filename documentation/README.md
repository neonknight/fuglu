# sphinx doc syntax
```
https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
https://sphinx-tutorial.readthedocs.io/step-1/
https://pythonhosted.org/an_example_pypi_project/sphinx.html
https://documentation-style-guide-sphinx.readthedocs.io/en/latest/style-guide.html
```

# build doc locally
specially helpful to develop locally before submit pr. two steps.
1. build documentation in docker environment. documentation available in public/
2. cleanup python environment and revert changes

build doc
```
docker run --rm --name fuglu_doc -it -v ~/fuglu:/fuglu debian:12
apt-get update
apt-get install -y python3 python3-pip python3-venv
python3 -m venv doc_env
source doc_env/bin/activate
pip install domainmagic python-libmilter sphinx
cd /fuglu/fuglu/develop/doc/
python3 make_sphinx_plugin_doc.py > ../../../documentation/source/includedplugins-autogen.txt
cd ../../../documentation
make html
mv _build/* ../public/
```

cleanup
```
$ sudo rm -rf fuglu/develop/doc/doc_env/ public/
$ git checkout documentation/source/includedplugins-autogen.txt
```
