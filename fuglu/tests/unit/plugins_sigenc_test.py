# -*- coding: UTF-8 -*-
import os

from .unittestsetup import TESTDATADIR
import unittest
from fuglu.plugins.sigenc import LoadSMIMECert, LearnSMIMECert, SignEncrypt, DecryptVerify, HAVE_ENDESIVE
from fuglu.shared import Suspect, FuConfigParser
import shutil
import tempfile
import os


class SigEncTestCase(unittest.TestCase):
    def setUp(self) -> None:
        _, tempfilename = tempfile.mkstemp(suffix='smime', prefix='fuglu-unittest', dir='/tmp')
        self.tempfilename = tempfilename
        self.tempdir = tempfile.mkdtemp(suffix='smime', prefix='fuglu-unittest', dir='/tmp')
        
    def tearDown(self) -> None:
        if os.path.exists(self.tempfilename):
            os.remove(self.tempfilename)
        if os.path.exists(self.tempdir):
            shutil.rmtree(self.tempdir)
    
    def _get_loader_plugin(self):
        cfg = FuConfigParser()
        cfg.add_section('loader')
        cfg.set('loader', 'certificate_store', TESTDATADIR + '/sigenc/')
        loader = LoadSMIMECert(cfg, 'loader')
        return loader
    
    def _get_learner_plugin(self):
        cfg = FuConfigParser()
        cfg.add_section('learner')
        cfg.set('learner', 'pubkey_store', self.tempdir)
        loader = LearnSMIMECert(cfg, 'learner')
        return loader
    
    def _get_sigenc_plugin(self):
        cfg = FuConfigParser()
        cfg.add_section('sigenc')
        candidate = SignEncrypt(cfg, 'sigenc')
        return candidate
    
    def _get_decver_plugin(self):
        cfg = FuConfigParser()
        cfg.add_section('decver')
        cfg.set('decver', 'ca_store', TESTDATADIR + '/sigenc/ca.crt')
        candidate = DecryptVerify(cfg, 'decver')
        return candidate
    
    def _get_textpart(self, msgrep):
        textpart = None
        for part in msgrep.walk():
            if part.get_content_type() == 'text/plain':
                textpart = part.as_bytes()
        return textpart
    
    
    def test_lint_loader(self):
        loader = self._get_loader_plugin()
        ok = loader.lint()
        self.assertTrue(ok)
        
    def test_lint_learner(self):
        learner = self._get_learner_plugin()
        ok = learner.lint()
        self.assertTrue(ok)
        
    def test_lint_sigenc(self):
        if not HAVE_ENDESIVE:
            return
        sigenc = self._get_sigenc_plugin()
        ok = sigenc.lint()
        self.assertTrue(ok)
    
    def test_lint_decver(self):
        if not HAVE_ENDESIVE:
            return
        decver = self._get_decver_plugin()
        ok = decver.lint()
        self.assertTrue(ok)
    
    def test_roundtrip(self):
        if not HAVE_ENDESIVE:
            return
        shutil.copy(TESTDATADIR + '/helloworld.eml', self.tempfilename)
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', self.tempfilename)
        orig_textpart = self._get_textpart(suspect.get_message_rep())
        self.assertIsNotNone(orig_textpart)
        
        loader = self._get_loader_plugin()
        loader.examine(suspect)
        self.assertIn('smime.pubkeys', suspect.tags)
        self.assertIn('sender@unittests.fuglu.org', suspect.tags['smime.pubkeys'])
        self.assertIn('rcpt@unittests.fuglu.org', suspect.tags['smime.pubkeys'])
        self.assertIn('smime.privkeys', suspect.tags)
        self.assertIn('sender@unittests.fuglu.org', suspect.tags['smime.privkeys'])
        self.assertIn('rcpt@unittests.fuglu.org', suspect.tags['smime.privkeys'])
        
        candidate = self._get_sigenc_plugin()
        fromaddr = candidate._get_fromaddr(suspect)
        self.assertEqual(fromaddr, 'sender@unittests.fuglu.org')
        signed = candidate.sign_smime(suspect, fromaddr)
        self.assertTrue(signed)
        encrypted = candidate.encrypt_smime(suspect)
        self.assertTrue(encrypted)
        self.assertNotEqual(suspect.get_source(), suspect.get_original_source())
        
        candidate = self._get_decver_plugin()
        decrypted = candidate.decrypt_smime_data(suspect)
        self.assertTrue(decrypted)
        answer = candidate.verify_smime_sig(suspect)
        self.assertTrue(answer.signed)
        self.assertTrue(answer.signature_ok)
        self.assertTrue(answer.certificate_ok)
        self.assertIsNone(answer.error)
        self.assertIn('sender@unittests.fuglu.org', answer.signer_certs)
        
        textpart = self._get_textpart(suspect.get_message_rep())
        self.assertEqual(textpart, orig_textpart)
        
        
        