# -*- coding: UTF-8 -*-
import unittest
from fuglu.shared import Suspect, FuConfigParser
from fuglu.extensions.sql import get_session, DBConfig, SQL_EXTENSION_ENABLED, text, sql_alchemy_version, SQL_ALCHEMY_V1, SQL_ALCHEMY_V2

import traceback



class DBConfigTestCase(unittest.TestCase):

    """Test Templates"""

    def setUp(self):
        self.connectString = "mysql://testuser:mysql123@mariadb:3306/testdb?charset=utf8mb4"

        # To make sure tests don't interfere, use a unique identifier for the table created
        self.identifier = Suspect.generate_id()

        config = FuConfigParser()
        config.add_section('databaseconfig')
        config.set('databaseconfig', 'globalscope', '$GLOBAL')
        config.set('databaseconfig', 'dbpriority', 'sql')
        config.set('databaseconfig', 'dbconnectstring', self.connectString)
        config.set('databaseconfig', "sql",
                   f"SELECT COALESCE("
                   f"(select value from fugluconfig_{self.identifier} where section=:section and option=:option and scope=:to_address order by scope limit 1), "
                   f"(select value from fugluconfig_{self.identifier} where section=:section and option=:option and scope=concat('%', :to_domain) order by scope limit 1),"
                   f"(select value from fugluconfig_{self.identifier} where section=:section and option=:option and scope='$GLOBAL' order by scope limit 1))"
                   "as value;"
        )
        self.config = config
        self.create_table()

    def create_table(self):
        sql = f"""
        CREATE TABLE fugluconfig_{self.identifier} (
           scope varchar(255) NOT NULL,
           section varchar(255) NOT NULL,
           option varchar(255) NOT NULL,
           value varchar(255) NOT NULL
        )
        """
        self.exec_sql(sql)

    def exec_sql(self, sql, values=None):
        if not SQL_EXTENSION_ENABLED:
            return
        if values is None:
            values = {}
        session = get_session(self.connectString)
        if sql_alchemy_version == SQL_ALCHEMY_V1:
            session.execute(text(sql), values)
            session.remove()
        elif sql_alchemy_version == SQL_ALCHEMY_V2:
            try:
                session.execute(text(sql), values)
                session.commit()
            except Exception:
                print(traceback.format_exc())
                session.rollback()
            finally:
                session.close()

    def clear_table(self):
        self.exec_sql(f"DELETE FROM fugluconfig_{self.identifier}")

    def insert_override(self, scope, section, option, value):
        sql = f"INSERT INTO fugluconfig_{self.identifier} (scope,section,option,value) VALUES (:scope,:section,:option,:value)"
        values = dict(scope=scope, section=section, option=option, value=value)
        self.exec_sql(sql, values)



    def test_user_override(self):
        """Test user config overrdide functionality"""
        if not SQL_EXTENSION_ENABLED:
            return
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')

        candidate = DBConfig(self.config, suspect)

        candidate.add_section('testsection')
        candidate.set('testsection', 'nooverride', '100')
        candidate.set('testsection', 'override', '100')

        self.clear_table()
        self.insert_override('recipient@unittests.fuglu.org', 'testsection', 'override', '200')
        self.insert_override('%unittests.fuglu.org', 'testsection', 'override', '300')
        self.insert_override('$GLOBAL', 'testsection', 'override', '400')
        self.assertEqual(int(candidate.get('testsection', 'nooverride')), 100)
        self.assertEqual(int(candidate.get('testsection', 'override')), 200)

    def test_domain_override(self):
        """Test domain config override functionality"""
        if not SQL_EXTENSION_ENABLED:
            return
        suspect = Suspect('sender@unittests.fuglu.org', 'someotherrec@unittests.fuglu.org', '/dev/null')

        candidate = DBConfig(self.config, suspect)

        candidate.add_section('testsection')
        candidate.set('testsection', 'nooverride', '100')
        candidate.set('testsection', 'override', '100')


        self.clear_table()
        self.insert_override('recipient@unittests.fuglu.org', 'testsection', 'override', '200')
        self.insert_override('%unittests.fuglu.org', 'testsection', 'override', '300')
        self.insert_override('$GLOBAL', 'testsection', 'override', '400')
        self.assertEqual(int(candidate.get('testsection', 'nooverride')), 100)
        self.assertEqual(int(candidate.get('testsection', 'override')), 300)

    def test_global_override(self):
        """Test global config override functionality"""
        if not SQL_EXTENSION_ENABLED:
            return
        suspect = Suspect('sender@unittests.fuglu.org', 'someotherrec@unittests2.fuglu.org', '/dev/null')

        candidate = DBConfig(self.config, suspect)

        candidate.add_section('testsection')
        candidate.set('testsection', 'nooverride', '100')
        candidate.set('testsection', 'override', '100')

        self.clear_table()
        self.insert_override('recipient@unittests.fuglu.org', 'testsection', 'override', '200')
        self.insert_override('%unittests.fuglu.org', 'testsection', 'override', '300')
        self.insert_override('$GLOBAL', 'testsection', 'override', '400')
        self.assertEqual(int(candidate.get('testsection', 'nooverride')), 100)
        self.assertEqual(int(candidate.get('testsection', 'override')), 400)

