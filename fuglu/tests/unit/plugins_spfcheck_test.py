# -*- coding: UTF-8 -*-
import unittest
from fuglu.plugins.domainauth import SPFPlugin, parse_received_xxx
from fuglu.shared import Suspect, DUNNO, REJECT, DEFER, FuConfigParser, actioncode_to_string, FileList, get_outgoing_helo
import copy
import spf
import dns.name
import typing as tp


TESTDOMAINS = {
    'hardfail.com': {'result': 'fail', 'action':REJECT},
    'hardfail-noselect.com': {'result': 'fail', 'action':REJECT, 'selective':False},
    'softfail.com': {'result': 'softfail', 'action':REJECT},
    'softfail-noselect.com': {'result': 'softfail', 'action':REJECT, 'selective':False},
    'temperr.com': {'result': 'temperr', 'action':DEFER},
    'pass.com': {'result': 'pass', 'action':DUNNO},
}


class FakeList(FileList):
    def __init__(self, config, section):
        super().__init__()
        self.filename = config.get(section, 'domain_selective_spf_file')
    
    def get_list(self):
        domains = []
        if self.filename:
            for domain in TESTDOMAINS.keys():
                if TESTDOMAINS[domain].get('selective', True):
                    domains.append(domain)
        return domains


class SPFPluginTest(SPFPlugin):
    def __init__(self, config, section=None):
        super().__init__(config, section)
        self.selective_domain_loader = FakeList(config, section)
    
    def _spf_lookup(self, sessid:str, query:spf.query, retries:int=3) -> tp.Tuple[str,str,str]:
        result = TESTDOMAINS.get(query.d, {}).get('result', 'temperr')
        return result, 'just a dummy explanation', "v=spf1 mx -all"
    
    def _get_clientinfo(self, suspect):
        ip = '8.8.8.8'
        helo = 'mail.example.com'
        revdns = 'mail.example.com'
        return ip, helo, revdns


class SPFTestCase(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def tearDown(self):
        pass
    
    def test_case1(self):
        testdomains = copy.deepcopy(TESTDOMAINS)
        testdomains['softfail.com']['action'] = DUNNO
        testdomains['softfail-noselect.com']['action'] = DUNNO
        
        config = FuConfigParser()
        section = 'SPFPlugin'
        config.add_section(section)
        config.set(section, 'domain_selective_spf_file', '')
        config.set(section, 'selective_softfail', 'False')
        config.set(section, 'on_fail', 'REJECT')
        config.set(section, 'on_softfail', 'DUNNO')
        config.set(section, 'on_temperr', 'DEFER')
        
        candidate = SPFPluginTest(config, section)
        for domain in testdomains:
            suspect = Suspect(f'sender@{domain}', 'recipient@unittests.fuglu.org', '/dev/null')
            action, message = candidate._run(suspect)
            exp_action = testdomains.get(domain, {}).get('action', None)
            
            self.assertIsNotNone(exp_action, f'expected action for {domain} was None')
            self.assertEqual(exp_action, action, f'action for {domain} expected {actioncode_to_string(exp_action)} but got {actioncode_to_string(action)} {message}')
    
    
    def test_case2(self):
        testdomains = copy.deepcopy(TESTDOMAINS)
        
        config = FuConfigParser()
        section = 'SPFPlugin'
        config.add_section(section)
        config.set(section, 'domain_selective_spf_file', '')
        config.set(section, 'selective_softfail', 'False')
        config.set(section, 'on_fail', 'REJECT')
        config.set(section, 'on_softfail', 'REJECT')
        config.set(section, 'on_temperr', 'DEFER')
        
        candidate = SPFPluginTest(config, section)
        for domain in testdomains:
            suspect = Suspect(f'sender@{domain}', 'recipient@unittests.fuglu.org', '/dev/null')
            action, message = candidate._run(suspect)
            exp_action = testdomains.get(domain, {}).get('action', None)
            
            self.assertIsNotNone(exp_action, f'expected action for {domain} was None')
            self.assertEqual(exp_action, action, f'action for {domain} expected {actioncode_to_string(exp_action)} but got {actioncode_to_string(action)} {message}')
    
    
    def test_case3(self):
        testdomains = copy.deepcopy(TESTDOMAINS)
        testdomains['hardfail-noselect.com']['action'] = DUNNO
        testdomains['softfail-noselect.com']['action'] = DUNNO
        
        config = FuConfigParser()
        section = 'SPFPlugin'
        config.add_section(section)
        config.set(section, 'domain_selective_spf_file', '/tmp/foobarbaz.txt')
        config.set(section, 'selective_softfail', 'False')
        config.set(section, 'on_fail', 'REJECT')
        config.set(section, 'on_softfail', 'REJECT')
        config.set(section, 'on_temperr', 'DEFER')
        
        candidate = SPFPluginTest(config, section)
        for domain in testdomains:
            suspect = Suspect(f'sender@{domain}', 'recipient@unittests.fuglu.org', '/dev/null')
            action, message = candidate._run(suspect)
            exp_action = testdomains.get(domain, {}).get('action', None)
            
            #print(suspect.get_tag("SPF.explanation"))
            self.assertIsNotNone(exp_action, f'expected action for {domain} was None')
            self.assertEqual(exp_action, action, f'action for {domain} expected {actioncode_to_string(exp_action)} but got {actioncode_to_string(action)} {message}')
    
    
    def test_case4(self):
        testdomains = copy.deepcopy(TESTDOMAINS)
        testdomains['softfail-noselect.com']['action'] = DUNNO
        
        config = FuConfigParser()
        section = 'SPFPlugin'
        config.add_section(section)
        config.set(section, 'domain_selective_spf_file', '/tmp/foobarbaz.txt')
        config.set(section, 'selective_softfail', 'True')
        config.set(section, 'on_fail', 'REJECT')
        config.set(section, 'on_softfail', 'REJECT')
        config.set(section, 'on_temperr', 'DEFER')
        
        candidate = SPFPluginTest(config, section)
        for domain in testdomains:
            suspect = Suspect(f'sender@{domain}', 'recipient@unittests.fuglu.org', '/dev/null')
            action, message = candidate._run(suspect)
            exp_action = testdomains.get(domain, {}).get('action', None)
            
            #print(suspect.get_tag("SPF.explanation"))
            self.assertIsNotNone(exp_action, f'expected action for {domain} was None')
            self.assertEqual(exp_action, action, f'action for {domain} expected {actioncode_to_string(exp_action)} but got {actioncode_to_string(action)} {message}')
    
    
    def test_case5(self):
        testdomains = copy.deepcopy(TESTDOMAINS)
        testdomains['softfail.com']['action'] = DUNNO
        testdomains['softfail-noselect.com']['action'] = DUNNO
        testdomains['hardfail-noselect.com']['action'] = DUNNO
        
        config = FuConfigParser()
        section = 'SPFPlugin'
        config.add_section(section)
        config.set(section, 'domain_selective_spf_file', '/tmp/foobarbaz.txt')
        config.set(section, 'selective_softfail', 'False')
        config.set(section, 'on_fail', 'REJECT')
        config.set(section, 'on_softfail', 'DUNNO')
        config.set(section, 'on_temperr', 'DEFER')
        
        candidate = SPFPluginTest(config, section)
        for domain in testdomains:
            suspect = Suspect(f'sender@{domain}', 'recipient@unittests.fuglu.org', '/dev/null')
            action, message = candidate._run(suspect)
            exp_action = testdomains.get(domain, {}).get('action', None)
            
            #print(suspect.get_tag("SPF.explanation"))
            self.assertIsNotNone(exp_action, f'expected action for {domain} was None')
            self.assertEqual(exp_action, action, f'action for {domain} expected {actioncode_to_string(exp_action)} but got {actioncode_to_string(action)} {message}')
    
    
    def test_selective(self):
        testdomains = copy.deepcopy(TESTDOMAINS)
        config = FuConfigParser()
        section = 'SPFPlugin'
        config.add_section(section)
        config.set(section, 'domain_selective_spf_file', '/tmp/foobarbaz.txt')
        
        candidate = SPFPluginTest(config, section)
        for domain in testdomains:
            should_select = testdomains[domain].get('selective', True)
            selected = candidate._check_domain_selective(domain)
            
            self.assertEqual(should_select, selected, f'{domain} selective: should={should_select} really={selected}')
        
        
    def test_hoster_mx_exception(self):
        config = FuConfigParser()
        section = 'SPFPlugin'
        config.add_section(section)
        
        domain = 'unittests.fuglu.org'
        host_name = 'mail-io1-xd33.google.com'
        clientip = '2607:f8b0:4864:20::d33' # some google ip
        
        candidate = SPFPluginTest(config, section)
        suspect = Suspect(f'sender@{domain}', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = host_name, clientip, host_name
        query = spf.query(clientip, f'sender@{domain}', host_name)
        spfrecord_mx = "v=spf1 mx -all"
        spfrecord_nomx = "v=spf1 -all"
        query.cache = {
            (domain, 'TXT'): [(spfrecord_mx.encode(),)],
            (domain, 'MX'): [(1, dns.name.from_text('aspmx.l.google.com.'),)],
        }
        result = candidate._hoster_mx_exception('<dummy>', spfrecord_mx, query, ['google.com'], host_name)
        self.assertTrue(result, 'hoster exception failed')
        
        
        query.cache = {
            (domain, 'TXT'): [(spfrecord_nomx.encode(),)],
            (domain, 'MX'): [(1, dns.name.from_text('aspmx.l.google.com.'),)],
        }
        result = candidate._hoster_mx_exception('<dummy>', spfrecord_nomx, query, ['google.com'], host_name)
        self.assertFalse(result, 'hoster exception passed (no mx)')
        
        query.cache = {
            (domain, 'TXT'): [(spfrecord_mx.encode(),)],
            (domain, 'MX'): [(1, dns.name.from_text('fuglu.org.'),)],
        }
        result = candidate._hoster_mx_exception('<dummy>', spfrecord_mx, query, ['google.com'], host_name)
        self.assertFalse(result, 'hoster exception passed (have mx)')
    
    
    def test_write_read_header(self):
        config = FuConfigParser()
        section = 'SPFPlugin'
        config.add_section(section)
        candidate = SPFPluginTest(config, section)
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        
        result = 'pass'
        explanation = 'just some random but rather long explanation'
        client_ip = '127.0.0.1'
        helo_name = 'mail.fuglu.org'
        
        header = candidate._create_received_spf(suspect, result, explanation, client_ip, helo_name)
        status, fieldmap, expl = parse_received_xxx(header)
        self.assertEqual(status.lower(), result, f'status not identical ({status} != {result})')
        self.assertEqual(explanation, expl, f'explanation not identical ({expl} != {explanation})')
        self.assertEqual(fieldmap.get('client-ip'), client_ip, f'client ip not identical ({fieldmap.get("client-ip")} != {client_ip})')
        self.assertEqual(fieldmap.get('helo'), helo_name, f'helo not identical ({fieldmap.get("helo")} != {helo_name})')
        self.assertEqual(fieldmap.get('receiver'), get_outgoing_helo(config), f'receiver not identical ({fieldmap.get("receiver")} != {get_outgoing_helo(config)})')
        
        
    
        