import unittest
import unittest.mock

import fuglu.connectors.asyncmilterconnector as asm
import fuglu.connectors.milterconnector as sm
from fuglu.mplugins.mpluginskipper import MilterPluginSkipper
from fuglu.shared import FuConfigParser


class MilterPluginSkipperTest(unittest.TestCase):
    """Test milter plugin skipper plugin"""

    def test_lint_defaults(self) -> None:
        conf = FuConfigParser()
        conf.add_section('test')

        plug = MilterPluginSkipper(conf, section="test")
        self.assertTrue(plug.lint())

    def test_lint(self) -> None:
        conf = FuConfigParser()
        conf.add_section('test')
        conf.set('test', 'skiplist', 'PlugA, PlugB,PlugC')

        plug = MilterPluginSkipper(conf, section="test")
        self.assertTrue(plug.lint())

    def test_extract(self) -> None:
        conf = FuConfigParser()
        conf.add_section('test')
        conf.set('test', 'skiplist', 'PlugA, PlugB,PlugC')

        plug = MilterPluginSkipper(conf, section="test")

        skiplist = plug._get_skiplist()
        self.assertEqual(sorted(["PlugA", "PlugB", "PlugC"]), sorted(skiplist))


    def test_full(self) -> None:
        """End to end skip test using skip check function applied in n routines"""
        conf = FuConfigParser()
        conf.add_section('test')
        conf.set('test', 'skiplist', 'PlugA, PlugB,PlugC')

        plug = MilterPluginSkipper(conf, section="test")

        sess = asm.MilterSession(unittest.mock.MagicMock(), unittest.mock.MagicMock())
        result = plug.examine_connect(sess=sess, host=b"localhost", addr=b"127.0.0.1")
        self.assertEqual(sm.CONTINUE, result)

        for plugin in ("PlugA", "PlugB", "PlugC"):
            self.assertTrue(sess.skip_plugin(plugin))
        self.assertFalse(sess.skip_plugin("PlugD"))