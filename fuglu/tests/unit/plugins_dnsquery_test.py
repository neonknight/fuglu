# -*- coding: UTF-8 -*-
import unittest
from fuglu.shared import Suspect, DUNNO, REJECT, FuConfigParser, _SuspectTemplate
from fuglu.plugins.dnsdata import DNSData, GetOrigin
from fuglu.lib.patchedemail import PatchedMessage
from .unittestsetup import TESTDATADIR
import os


class DNSDataTestCase(unittest.TestCase):
    def setUp(self):
        config = FuConfigParser()
        config.add_section('DNSData')
        self.candidate = DNSData(config)

    def tearDown(self):
        pass
    
    def test_lint(self):
        result = self.candidate.lint()
        self.assertTrue(result)
    
    def test_sort_mx(self):
        data = ['40 alt4.gmail-smtp-in.l.google.com.', '30 alt3.gmail-smtp-in.l.google.com.', '20 alt2.gmail-smtp-in.l.google.com.', '10 alt1.gmail-smtp-in.l.google.com.', '5 gmail-smtp-in.l.google.com.']
        expect = ['gmail-smtp-in.l.google.com.', 'alt1.gmail-smtp-in.l.google.com.', 'alt2.gmail-smtp-in.l.google.com.', 'alt3.gmail-smtp-in.l.google.com.', 'alt4.gmail-smtp-in.l.google.com.']
        output = self.candidate._sort_mx(data)
        self.assertEqual(output,expect)


class GetOriginTestCase(unittest.TestCase):
    def setUp(self):
        config = FuConfigParser()
        config.add_section('GetOrigin')
        config.set('GetOrigin', 'headers', 'X-Dummy-IP')
        self.candidate = GetOrigin(config)
    
    def tearDown(self):
        pass
    
    def test_lint(self):
        result = self.candidate.lint()
        self.assertTrue(result)
    
    def test_extract_hdr(self):
        testformats = ['${ip}', '${ip}!00000', '(XXXXXXX) [${ip}]:00000', '[${ip}]', 'IPv4[${ip}] Epoch[0000000000]', 'example.com/script.php for ${ip}', '/path/to/script.php REMOTE_ADDR: ${ip}', '${ip}, 10.2.3.4', '${ip} (${ip})']
        valid_ips = ['8.8.8.8', '2a00:1450:400a:803::200e']
        invalid_ips = ['127.0.0.1', '::1', '0000:0000:0000::0', '256.256.256.256', 'asdfjkl']
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        for fmt in testformats:
            t = _SuspectTemplate(fmt)
            for ip in valid_ips:
                val = t.safe_substitute({'ip':ip})
                msgrep = PatchedMessage()
                msgrep['X-Dummy-IP'] = val
                output = self.candidate._get_origin_hdr(msgrep, suspect)
                self.assertEqual(ip, output, f'failed to extract IP {ip} from {fmt} as {val}')
            for ip in invalid_ips:
                val = t.safe_substitute({'ip':ip})
                msgrep = PatchedMessage()
                msgrep['X-Dummy-IP'] = val
                output = self.candidate._get_origin_hdr(msgrep, suspect)
                self.assertIsNone(output, f'somehow extracted invalid IP {ip} from {fmt} as {val}')
    
    def test_extract_rcvd(self):
        """
        Test ip/ptr/helo extraction from received headers
        """
        headers = [
            ((None, None, None), 'from ZRAP278MB0803.CHEP278.PROD.OUTLOOK.COM ([fe80::50f5:d53c:703a:7cb5]) by ZRAP278MB0803.CHEP278.PROD.OUTLOOK.COM ([fe80::50f5:d53c:703a:7cb5%5]) with mapi id 15.20.7544.023; Wed, 1 May 2024 11:40:32 +0000'),
            ((None, None, None), 'by mail-lj1-x234.google.com with SMTP id 38308e7fff4ca-2dd6c14d000so82277691fa.0 for <rcpt@unittests.fuglu.org>; Wed, 01 May 2024 04:46:33 -0700 (PDT)'),
            ((None, None, None), 'from mail.biglobe.ne.jp by mta-snd-w06.biglobe.ne.jp with ESMTP id <20240501185328568.QOKA.102657.mail.biglobe.ne.jp@biglobe.ne.jp> for <rcpt@unittests.fuglu.org>; Thu, 2 May 2024 03:53:28 +0900'),
            (('8.8.8.8','somehosthelo',None), 'from somehosthelo ([8.8.8.8]) by smtp.gmail.com with ESMTPSA id n21-20020a05600c465500b0041be4065adasm1942166wmo.22.2024.05.01.04.52.13 (version=TLS1_3 cipher=TLS_AES_128_GCM_SHA256 bits=128/128); Wed, 01 May 2024 04:52:15 -0700 (PDT)'),
            (('8.8.8.8','somehosthelo',None), 'from somehosthelo ( [8.8.8.8]) by bizesmtp.qq.com (ESMTP) with id ; 周一, 22 7月 2024 17:47:32 +0800 (CST)'),
        ]
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        for exp, hdr in headers:
            msgrep = PatchedMessage()
            msgrep['Received'] = hdr
            origin, originhelo, originptr = self.candidate._get_origin_rcvd(msgrep, suspect)
            self.assertEqual(exp[0], origin, f'expected IP {exp[0]} got {origin}')
            self.assertEqual(exp[1], originhelo, f'expected IP {exp[1]} got {originhelo}')
            self.assertEqual(exp[2], originptr, f'expected IP {exp[2]} got {originptr}')
    
    
    def test_broken_rcvd(self):
        """
        test parsing of received headers with defects (e.g. unexpected utf8 characters)
        """
        filename = os.path.join(TESTDATADIR, 'badhdr-utf8-rcvd.eml')
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', filename)
        msgrep = suspect.get_message_rep()
        origin, originhelo, originptr = self.candidate._get_origin_rcvd(msgrep, suspect)
        self.assertEqual('8.8.8.8', origin, f'expected IP 8.8.8.8 got {origin}')
    
    
    def test_extra_rcvd(self):
        """
        Test parsing of received headers as they are written by exim and other MTA. more samples may be needed.
        """
        headers = [
            # exim
            (('helo.fuglu.org', None, '8.8.8.8', 'mail.fuglu.org'), 'from [8.8.8.8] (helo=helo.fuglu.org) by mail.fuglu.org (Exim) with esmtpsa (TLS1.2:ECDHE_SECP256R1__RSA_SHA512__AES_256_GCM:256) (envelope-from <sender@unittest.fuglu.org>) id 1t6hvS-00BEX5-1R for recipient@unittest.fuglu.org; Fri, 01 Nov 2024 04:02:15 +0100'),
            ((None, 'revdns.fuglu.org', '8.8.8.8', 'mail.fuglu.org'), 'from revdns.fuglu.org ([8.8.8.8]:55584) by mail.fuglu.org with esmtpa (Exim 4.93) (envelope-from <sender@unittest.fuglu.org>) id 1t6lz4-002HTH-OJ for recipient@unittest.fuglu.org; Fri, 01 Nov 2024 08:22:15 +0100'),
            (('helo.fuglu.org', None, '8.8.8.8', 'mail.fuglu.org'), 'from [8.8.8.8] (port=50084 helo=helo.fuglu.org) by mail.fuglu.org with esmtpsa (TLS1.2) tls TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (Exim 4.96.2) (envelope-from <sender@unittest.fuglu.org>) id 1t6qsV-0009dq-2T for recipient@unittest.fuglu.org; Fri, 01 Nov 2024 08:35:48 -0400'),
            (('helo.fuglu.org', 'revdns.fuglu.org', '8.8.8.8', 'mail.fuglu.org'), 'from revdns.fuglu.org ([8.8.8.8] helo=helo.fuglu.org) by mail.fuglu.org with esmtpa (Exim 4.98) (envelope-from <sender@unittest.fuglu.org>) id 1tX6w9-000000000I3-1F0e for recipient@unittest.fuglu.org; Sun, 12 Jan 2025 23:00:05 +0000'),
            # other mua
            (('helo.fuglu.org', None, '8.8.8.8', 'mail.fuglu.org'), 'from [8.8.8.8] ([8.8.8.8:50302] helo=helo.fuglu.org) by mail.fuglu.org (envelope-from <sender@unittest.fuglu.org>) (ecelerity 4.4.1.20033 r(msys-ecelerity:tags/4.4.1.0^0)) with ESMTPS (cipher=DHE-RSA-AES256-GCM-SHA384) id 6B/CA-20522-B29C6776; Thu, 02 Jan 2025 12:13:16 -0500'),
            # squirrelmail
            ((None, None, '8.8.8.8', 'mail.fuglu.org'), 'from 8.8.8.8 (SquirrelMail authenticated user h0595bbt) by mail.fuglu.org with HTTP; Mon, 13 Jan 2025 01:00:56 +0100'),
        
        ]
        for exp, hdr in headers:
            msgrep = PatchedMessage()
            msgrep['Received'] = hdr
            values = self.candidate._parse_rcvd_header_extra(hdr)
            self.assertEqual(exp, values, f'failed to find data in header {hdr}')
    